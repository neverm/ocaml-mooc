let rec gcd n m = if m = 0 then n else gcd m (n mod m);; 

let multiple_upto n r =
  let rec test_divisor d = if d > r then false
    else if multiple_of n d then true else test_divisor (d + 1)
  in
  test_divisor 2;;

let is_prime n =
  if n < 2 then false else not (multiple_upto n (integer_square_root n))
