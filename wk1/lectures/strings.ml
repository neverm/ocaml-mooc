let sentence =
  let commaed = "," ^ word in
  let double = commaed ^ commaed in 
  let quad = double ^ double in
  let oct = quad ^ quad in
  word ^ oct;;