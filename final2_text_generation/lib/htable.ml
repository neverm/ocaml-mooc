type distribution = {
  total: int;
  amounts: (string * int) list
}

type t = (string, distribution) Hashtbl.t

let get_or_else table key default = match Hashtbl.find_opt table key with
  | Some value -> value
  | None -> default

(* update frequency value of given word in the distribution by one occurrence *)
let update_distribution table word =
  let current_frequency = get_or_else table word 0 in
  Hashtbl.replace table word (current_frequency + 1)

let compute_distribution words =
  let table = Hashtbl.create (List.length words / 10) in
  (* let words_sorted = List.sort String.compare words in *)
  let rec fill_table = function
    | [] -> ()
    | word :: rest ->
      update_distribution table word;
      fill_table rest
  in
  fill_table words;
  table
  

let build_htable words = []

(* randomly select a successor for the given word in the given table *)
let next_in_htable table word = ""

(* takes a table and returns a sequence of words that form a valid random sentence *)
let walk_htable table = []

let distr = compute_distribution ["a"; "b"; "a"]
