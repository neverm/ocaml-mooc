type t

val build_ltable: string list -> t

(* randomly select a successor for the given word in the given table *)
val next_in_ltable: t -> string -> string

(* takes a table and returns a sequence of words that form a valid random sentence *)
val walk_ltable: t -> string list
