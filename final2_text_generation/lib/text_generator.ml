let ($^) s c = s ^ (String.make 1 c)

(* Extract list of words from given string *)
let words str =
  
  (* For now, words will be alphanumeric characters (a-zA-Z0-9) *)

  (* true if given char is a part of some word *)
  let match_char c =
    (c >= 'a' && c <= 'z') ||
    (c >= 'A' && c <= 'Z') ||
    (c >= '0' && c <= '9')
  in

  let match_nonword_char c = not (match_char c)
  in

  (* Start consuming chars at pos, accumulating them into a string acc,
     until predicate returns false. Then return accumulated string and first
     unmatched position (for which predicate returned false)*)
  let rec consume_until acc pos predicate =
    if (pos >= String.length str) then
      (pos, acc)
    else
      let c = str.[pos] in
      if predicate c then
        consume_until (acc $^ c) (pos + 1) predicate
      else
        (pos, acc)
  in


  (* consume words into acc starting at position *)
  let rec consume_words acc pos =
    if (pos >= String.length str) then
      acc
    else
      let (last_pos, word) = consume_until "" pos match_char in
      if word = "" then
        let (last_pos, _) = consume_until "" pos match_nonword_char in
        consume_words acc last_pos
      else
        consume_words (word :: acc) last_pos
  in
  List.rev (consume_words [] 0)

let rec join_words = function
  | [] -> ""
  | word :: [] -> word
  | word :: rest -> word ^ " " ^ join_words rest

let generate_sentence () =
  let text = "I am a man and my dog is a good dog and a good dog makes a good man" in
  let tbl = Ltable.build_ltable (words text) in
  let words_list = Ltable.walk_ltable tbl in
  join_words words_list
