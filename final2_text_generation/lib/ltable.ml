type t = (string * string list) list

let build_ltable words =

  let add_entry table word successor =
  match List.assoc_opt word table with
  | None -> (word, [successor]) :: table
  | Some successors ->
    (word, successor :: successors)
    :: List.remove_assoc word table
  in

  let rec aux table words = match words with
  | word :: [] ->
    add_entry table word "STOP"
  | word :: successor :: rest ->
    aux (add_entry table word successor) (successor :: rest)
  | [] -> table
  in
  
  match words with
  | [] -> []
  | first_word :: _ -> add_entry (aux [] words) "START" first_word
    

(* randomly select a successor for the given word in the given table *)
let next_in_ltable table word =
  match List.assoc_opt word table with
  | Some successors when List.length successors = 0 ->
    raise Not_found
  | Some successors ->
    let n = List.length successors in
    List.nth successors (Random.int n)
  | None -> raise Not_found

(* takes a table and returns a sequence of words that form a valid random sentence *)
let walk_ltable table =

  let first_word = next_in_ltable table "START" in

  let rec find_next_word acc current_word =
    if current_word = "STOP" then
      acc
    else
      find_next_word (current_word :: acc) (next_in_ltable table current_word)
  in
  List.rev (find_next_word [] first_word)
