type t

val build_htable: string list -> t

(* randomly select a successor for the given word in the given table *)
val next_in_htable: t -> string -> string

(* takes a table and returns a sequence of words that form a valid random sentence *)
val walk_htable: t -> string list
