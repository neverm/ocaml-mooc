(* Add code to test as a library here *)
(* open Lib.Trie *)
open Lib.Ltable
open OUnit

let suite = 
  "Ltable" >::: [
    "test" >:: ( fun _ ->
      (* some passing test *)
        let tbl = build_ltable ["a"; "b"; "a"] in
        let one = 1 in
        let two = 2 in
        assert_equal (one+two) 3
    );

  ]

let _ = run_test_tt_main suite
  
