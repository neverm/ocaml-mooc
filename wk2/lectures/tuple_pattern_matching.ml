let x = 6 * 3 in x;;

let _ = 6 * 3 in 1;;

let (x, _) = (6 * 3, 2) in x;;

type point2d = int * int;;

let abscissa (x, _) = x;;

let ordinate (_, y) = x;;