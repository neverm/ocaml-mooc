let literal = [|1 ; 2; 3|];;

let five_ones = Array.make 5 1;;

let square x = x * x;;

let squares n = Array.init n square;;

let first_five_squares = squares 5;;

let len = Array.length five_ones;;

let third_one = five_ones.(2);;
