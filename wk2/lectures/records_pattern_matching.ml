type point2D = {x: int; y: int};;

let origin = {x = 0; y = 0};;

(* {x; y} as a shortcut for {x = x; y = y} *)
let from_tuple (x, y) = {x; y};;

type box = {
    upper_left: point2D;
    lower_right: point2D;
};;

let a: point2D = from_tuple(4, 2);;

let b: point2D = from_tuple(5, 6);;

let the_box = {upper_left = a; lower_right = b};;

let get_min_x { left_upper_corner = { x }} = x;;

let { left_upper_corner = { x }} = the_box in x;;