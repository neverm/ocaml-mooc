type phone_number = int * int * int * int;;

type contact = {
    name : string;
    phone_number: phone_number;
};;

let nobody = { name = ""; phone_number=(0, 0, 0, 0)};;

type database = {
    number_of_contacts: int;
    contacts: contact array;
};;

let make max_count =
    {
        number_of_contacts = 0;
        contacts = Array.make max_count nobody
    };;

type query = {
    code: int;
    contact: contact;
};;

let search db contact =
    let rec aux idx =
        if idx >= db.number_of_contacts then
            (false, db, nobody)
        else if db.contacts.(idx).name = contact.name then
            (true, db, db.contacts.(idx))
        else
            aux (idx + 1)
    in
    aux 0;;

let insert db contact =
    if db.number_of_contacts >= Array.length db.contacts
    then
        (false, db, nobody)
    else
        let (status, db, _) = search db contact in
        if status then (false, db, contact) else
        (* this function either looks up contact by it's index number,
        or returns the contact to be inserted, in case the index is fist empty position*)
        let cells i =
        if i = db.number_of_contacts then
            contact
        else db.contacts.(i)
    in
        let db' = {
            number_of_contacts = db.number_of_contacts + 1;
            contacts = Array.init (Array.length db.contacts) cells
        }
        in
        (true, db', contact);;

let delete db contact =
  let (status, db, _) = search db contact in
  if not status then (false, db, contact) else
    let rec aux idx =
      if idx >= db.number_of_contacts then
        -1
      else if db.contacts.(idx).name = contact.name then
        idx
      else
        aux (idx + 1)
    in
    let target_idx = aux 0 in
    (* get every record before deleted, then shift records by 1. But, when moving to unallocated part, remove the shift *)
    let cells i = db.contacts.(if i >= target_idx && i < db.number_of_contacts then i + 1 else i)
    in
    let db' = {
      number_of_contacts = db.number_of_contacts - 1;
      contacts = Array.init (Array.length db.contacts) cells
    }
    in (true, db', contact);;

let update db contact =
    let (status, db, _) = search db contact in
    if not status
    then insert db contact
    else
    let (_, db', _) = delete db contact in
    insert db' contact

let engine db {code, contact} =
    if code = 0 then insert db contact
    else if code = 1 then delete db contact
    else if code = 2 then search db contact
    else if code = 3 then update db contact
    else (false, db, nobody);;

let db = make 5;;

let (status, db, contact) = engine db (0, { name = "luke"; phone_number=(1, 2, 3, 4)});;

let (status, db, contact) = engine db (0, { name = "darth"; phone_number=(1, 1, 3, 3)});;

let (status, db, contact) = engine db (2, { name = "luke"; phone_number=(1, 2, 3, 4)});;

let (status, db, contact) = engine db (2, { name = "vitalya"; phone_number=(1, 2, 3, 4)});;

print_string (string_of_bool status);;

[|
    {code = 0; contact = { name = "bitalya"; phone_number=(1, 2, 3, 4)}};
    {code = 0; contact = { name = "bladimir"; phone_number=(1, 2, 3, 4)}};
    {code = 1; contact = { name = "bitalya"; phone_number=(1, 2, 3, 4)}};
    {code = 3; contact = { name = "bladimir"; phone_number=(1, 2, 3, 4)}};
|];;