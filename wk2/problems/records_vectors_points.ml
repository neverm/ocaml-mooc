type point  = { x : float; y : float; z : float }
type dpoint = { dx : float; dy : float; dz : float }
type physical_object = { position : point; velocity : dpoint }

let move p dp = {x = p.x +. dp.dx; y = p.y +. dp.dy; z = p.z +. dp.dz};;

let next obj = {position = move obj.position obj.velocity; velocity = obj.velocity};;

let distance p1 p2 = 
  let dx = p1.x -. p2.x and dy = p1.y -. p2.y and dz = p1.z -. p2.z in
  sqrt(dz ** 2. +. dy ** 2. +. dx ** 2.);;

let intersects pos1 pos2 = distance pos1 pos2 <= 2.0;;

let will_collide_soon p1 p2 =
  let next_p1 = next p1 and next_p2 = next p2 in
  intersects next_p1.position next_p2.position;;
