let exchange k = (k mod 10) * 10 + k / 10;;

let is_valid_answer (grand_father_age, grand_son_age) =
  (grand_son_age * 4 = grand_father_age) &&
  (exchange grand_son_age = 3 * (exchange grand_father_age));;

let find (max_grand_father_age, min_grand_son_age) =
  let rec find_inner (f_age, s_age) = 
    if s_age < min_grand_son_age ||
       f_age > max_grand_father_age ||
       s_age > f_age
    then
      (-1, -1)
    else
    if is_valid_answer (f_age, s_age)
    then (f_age, s_age)
    else
    if f_age = 10 then (-1, -1)
    else
    if s_age = f_age then find_inner (f_age - 1, min_grand_son_age)
    else find_inner (f_age, s_age + 1)
  in
  find_inner (max_grand_father_age, min_grand_son_age);;
