let min a =
  let rec find_min idx current_min =
    if idx >= Array.length a then current_min
    else
      let current = a.(idx) in
      if current < current_min then
        find_min (idx+1) current
      else find_min (idx+1) current_min
  in
  find_min 0 a.(0);;

let min_index a =
  let rec find_min idx current_min min_idx=
    if idx >= Array.length a then min_idx
    else
      let current = a.(idx) in
      if current < current_min then
        find_min (idx+1) current idx
      else find_min (idx+1) current_min min_idx
  in
  find_min 0 a.(0) 0;;

let it_scales = "no";;
