type date =
  { year : int; month : int; day : int;
    hour : int; minute : int }

let the_origin_of_time =
  { year = 1; month = 1; day = 1;
    hour = 0; minute = 0 }

let wellformed {year = y; month = m; day = d; hour = h; minute = mn} =
  y >= 1 &&
  (m >= 1 && m <= 5) &&
  (d >= 1 && d <= 4) &&
  (h >= 0 && h <= 2) &&
  (mn <= 1 && mn >= 0);;

let add_year {year; month; day; hour; minute} = 
  {year = year + 1; month; day; hour; minute};; 

let add_month {year; month; day; hour; minute} =
  if month < 5
  then {year; month = month + 1; day; hour; minute}
  else add_year {year; month = 1; day; hour; minute};;

let add_day {year; month; day; hour; minute} =
  if day < 4
  then {year; month; day = day + 1; hour; minute}
  else add_month {year; month; day = 1; hour; minute};;

let add_hour {year; month; day; hour; minute} =
  if hour < 2
  then {year; month; day; hour = hour + 1; minute}
  else add_day {year; month; day; hour = 0; minute};; 

let next {year; month; day; hour; minute} =
  if minute < 1
  then {year; month; day; hour; minute = 1}
  else add_hour {year; month; day; hour; minute = 0};; 

let mins_hour = 2;;
let mins_day = 3 * mins_hour;;
let mins_month = 4 * mins_day;;
let mins_year = 5 * mins_month;;

let of_int minutes =
  let years = minutes / mins_year and rest = minutes mod mins_year in
  let months = rest / mins_month and rest = rest mod mins_month in
  let days = rest / mins_day and rest = rest mod mins_day in
  let hours = rest / mins_hour and rest = rest mod mins_hour in
  {year=1+years; month=1+months; day=1+days; hour=hours; minute = rest};;
