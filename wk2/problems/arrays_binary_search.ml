let is_sorted a =
  let rec cmp_with_next idx =
    if idx + 1 >= Array.length a then true
    else if String.compare a.(idx) a.(idx+1) >= 0 then false
    else cmp_with_next (idx + 1)
  in cmp_with_next 0

let find dict word =
  let rec find_inner start _end = 
    let mid = (start + _end) / 2 in
    let mid_el = dict.(mid) in
    if String.compare mid_el word = 0 then mid
    else if start = _end then -1 else
    if String.compare mid_el word < 0
    then find_inner (mid+1) _end
    else find_inner start (mid-1)
  in
  let size = Array.length dict in
  if size > 0 then find_inner 0 (size-1)
  else -1
