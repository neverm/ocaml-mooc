module type GenericTrie = sig
  type 'a char_table
  type 'a trie = Trie of 'a option * 'a trie char_table
  val empty : unit -> 'a trie
  val insert : 'a trie -> string -> 'a -> 'a trie
  val lookup : 'a trie -> string -> 'a option
end

module CharHashedType =
struct
  type t = char
  let equal i j = i=j
  let hash c = Char.code c
end

module CharHashtbl = Hashtbl.Make(CharHashedType)

module Trie : GenericTrie
  with type 'a char_table = 'a CharHashtbl.t =
struct
  type 'a char_table = 'a CharHashtbl.t
  type 'a trie = Trie of 'a option * 'a trie char_table

  let initial_size = 100

  let empty_table () = CharHashtbl.create initial_size

  let empty () = Trie (None, empty_table ())

  let lookup trie w =
    let rec aux (Trie (value, children)) i =
      if i > String.length w then
        None
      else if i = String.length w then
        value
      else
        let c = w.[i] in
        let next = CharHashtbl.find_opt children c in
        match next with
        | None -> None
        | Some child -> aux child (i + 1)
    in
    aux trie 0

  let update_children children c new_trie =
    let new_tbl = CharHashtbl.copy children in
    CharHashtbl.add new_tbl c new_trie;
    new_tbl

  let insert trie w v =
    let rec inner (Trie (value, children)) i =
      let c = w.[i] in
      let is_end = (i = (String.length w - 1)) in
      let next_trie = match CharHashtbl.find_opt children c with
        | Some (Trie (next_val, next_children) as t') ->
          if is_end then
            Trie (Some v, CharHashtbl.copy next_children)
          else
            inner t' (i+1)
        | None ->
          if is_end then
            Trie (Some v, empty_table ())
          else
            inner (empty ()) (i + 1)
      in
      Trie (value, update_children children c next_trie)
    in
    if (String.length w) = 0 then
      trie
    else
      inner trie 0
end
