module type MultiSet_S = sig

  (* A multi-set of type ['a t] is a collection of values of
     type ['a] that may occur several times. *)
  type 'a t

  (* [occurrences s x] return the number of time [x] occurs
     in [s]. *)
  val occurrences : 'a t -> 'a -> int

  (* The empty set has no element. There is only one unique
     representation of the empty set. *)
  val empty : 'a t

  (* [insert s x] returns a new multi-set that contains all
     elements of [s] and a new occurrence of [x]. Typically,
     [occurrences s x = occurrences (insert s x) x + 1]. *)
  val insert : 'a t -> 'a -> 'a t

  (* [remove s x] returns a new multi-set that contains all elements
     of [s] minus an occurrence of [x] (if [x] actually occurs in
     [s]). Typically, [occurrences s x = occurrences (remove s x) x -
     1] if [occurrences s x > 0]. *)
  val remove : 'a t -> 'a -> 'a t

end

module MultiSet: MultiSet_S = struct

  type 'a t = ('a * int) list

  let rec occurrences mset x = match mset with
    | [] -> 0
    | (el, freq) :: _ when el = x -> freq
    | _ :: rest -> occurrences rest x

  let empty = []

  let remove mset x =
    let p = fun (el, occ) -> el != x in
    List.filter p mset

  let insert mset x =
    let old_occ = occurrences mset x in
    (x, old_occ+1) :: (remove mset x)
    
end

let letters word =
  let open MultiSet in
  let rec aux i mset =
    if i >= String.length word then
      mset
    else
      aux (i + 1) (insert mset word.[i])
  in
  aux 0 empty

let anagram word1 word2 =
  let open MultiSet in
  let ls1 = letters word1 and ls2 = letters word2 in
  let rec aux i =
    if i >= String.length word1 then
      true
    else
      let c = word1.[i] in
      if (occurrences ls1 c) != (occurrences ls2 c) then
        false
      else
        aux (i + 1)
  in
  aux 0

