module Stack = struct
  exception Empty_stack
  type 'a t = 'a list
  let empty = []
  let push x s = x :: s
  let pop = function
    | [] -> raise Empty_stack
    | x :: xs -> x
  let maybe_pop s =
    try
      Some (pop s)
    with
      Empty_stack -> None
end

let s = Stack.empty
