let rec last_element = function
  | x :: [] -> x
  | x :: rest -> last_element rest
  | _ -> (invalid_arg "last_element")

let rec is_sorted = function
  | [] -> true
  | x :: y :: rest when x > y -> false
  | x :: rest -> is_sorted rest
