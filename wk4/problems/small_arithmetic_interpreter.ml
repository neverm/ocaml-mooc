type operation =
  | Op of string * operation * operation
  | Value of int

type env = (string * (int -> int -> int)) list

let rec lookup_function name env = match env with
  | (name', f) :: env' -> if name = name' then f else lookup_function name env'
  | [] -> invalid_arg "lookup_function"

let add_function name f env = (name, f) :: env
  
let my_env = [ ("min", fun x y -> if x > y then y else x) ]

let rec compute env op = match op with
  | Value x -> x
  | Op (name, left, right) ->
    let f = lookup_function name env in
    f (compute env left) (compute env right)

(* question 5 makes no sense to me and most of the people just reused compute
function *)
