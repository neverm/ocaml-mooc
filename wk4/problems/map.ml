type 'a tree =
    Node of 'a tree * 'a * 'a tree
  | Leaf of 'a

let wrap l = List.map (fun x -> [x]) l

let rec tree_map f t = match t with
  | Leaf x -> Leaf (f x)
  | Node (left, x, right) ->
    Node (tree_map f left, (f x), tree_map f right)
