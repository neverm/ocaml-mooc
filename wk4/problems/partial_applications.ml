(* Every triangle has a circumscribed circle, that is a circle
   that goes through the three points of a given triangle.
Here we find the radius of this circle given angles a, b, c and circumference s*)

let pi_h = acos 0.
let degree_of_radian x = x *. 90. /. pi_h
let radian_of_degree x = x *. pi_h /. 90.

let process_angle angle = 2.0 *. cos ((radian_of_degree angle) /. 2.0)
                         
let ccr a b c s =
  s /. ((process_angle a) *. (process_angle b) *. (process_angle c))

let ccr_curried =
  fun a -> let a_processed = process_angle a in
    fun b -> let b_processed = process_angle b in
      fun c -> let c_processed = process_angle c in
        fun s -> s /. (a_processed *. b_processed *. c_processed)

