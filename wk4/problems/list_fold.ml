let rec filter p = function
  | [] -> []
  | x :: rest -> if p x then x :: filter p rest else filter p rest

let partition p l =
  let partition_element el (matched, unmatched) =
    if p el then (el :: matched, unmatched) else (matched, el :: unmatched) in
  List.fold_right partition_element l ([], [])

let rec sort = function
  | [] -> []
  | head :: rest ->
    let (less, greater_eq) = partition (fun el -> el < head) rest in
    sort less @ head :: sort greater_eq
