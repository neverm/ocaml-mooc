let read_lines =
  let sl = ref [] in
  let rec aux () =
    try
      sl := read_line () :: !sl ;
      aux ()
    with
      End_of_file -> List.rev !sl in
  fun () ->
    (* this is the "fix" so to speak, otherwise the state of sl is shared between calls *)
    sl := []; 
    aux ()

let read_and_print () =
  let strs = read_lines () in
  print_endline (List.fold_right (^) strs "")

let _ =
  read_and_print ();
  read_and_print ()
