type stack = int array
exception Full
exception Empty

let capacity st = Array.length st - 1

let create size = Array.make (size + 1) 0

let push st element =
  let size = st.(0) in
  if size = capacity st then
    raise Full
  else
    st.(size+1) <- element;
    st.(0) <- size + 1
  
let append st elements =
  for i = 0 to Array.length elements - 1 do
    push st elements.(i)
  done

let pop st =
  let stack_size = st.(0) in
  if stack_size = 0 then
    raise Empty
  else
    st.(0) <- stack_size - 1;
    st.(stack_size)
