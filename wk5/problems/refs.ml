exception Empty

let swap r1 r2 =
  let aux = ref !r1 in
  r1 := !r2;
  r2 := !aux

let update x f =
  let old = !x in
  x := f old;
  old

let move source destination =
  if List.length !source = 0 then raise Empty
  else
    let top = List.hd !source in
    source := List.tl !source;
    destination := top :: !destination

(*
Define reverse: 'a list -> 'a list, that has a type and an observable
behaviour that look like the ones of a purely functional function,
buf that use a reference internally to perform the computation.
It takes a list, and returns a copy of the list whose elements are in reverse order.
The only functions you can call, except from locally defined functions, are (!), (:=), ref,
and move that you just defined. And you are not allowed to use pattern matching.*)

(* without move  *)
let reverse list =
  let result = ref [] in
  let remains = ref list in
  while List.length !remains > 0 do
    result := (List.hd !remains) :: !result;
    remains := List.tl !remains
  done;
  !result

(* with move *)
let reverse2 list: ('a list) =
  let result = ref [] in
  let remains = ref list in
  try
  while true do
    move remains result
  done;
  !result
  with Empty -> !result
