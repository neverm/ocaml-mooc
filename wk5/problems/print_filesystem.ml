type filesystem = (string * node) list
and node =
  | File
  | Dir of filesystem
  | Symlink of string list

let rec string_join glue = function
  | [] -> ""
  | last :: [] -> last
  | part :: rest -> part ^ glue ^ (string_join glue rest)

let string_of_path path = string_join "/" path

let print_path path = print_string (string_of_path path)

let print_file depth filename =
  let depth_prefix =
    if depth > 0
    then
      let bars = List.init depth (fun _ -> "|") in
      string_join " " bars ^ " "
    else
      ""
  in
  print_string depth_prefix;
  print_string filename

let print_symlink depth name path =
  print_file depth name;
  print_string " -> ";
  print_path path

exception Invalid_path of string

let resolve path_to_symlink symlink_path =
  let sym_rev = List.rev path_to_symlink in
  (* add one ".." that will be used to omit the name of symlink *)
  let target = ".." :: symlink_path in
  let rec apply_relative path rel_path = match (path, rel_path) with
    | (p :: _, []) -> path
    | ([], r :: _) when r = ".." -> raise (Invalid_path (string_of_path rel_path))
    | (p :: rest_path, r :: rest_rel) ->
      if r = ".." then
        apply_relative rest_path rest_rel
      else
        apply_relative (r :: path) rest_rel
    | ([], rel_path) -> List.rev rel_path
  in
  List.rev (apply_relative sym_rev target)

let rec file_exists fs full_path = match (fs, full_path) with
  | ([], _) | (_, []) -> false
  | ((node_name, node) :: rest_fs, filename :: []) ->
    if node_name = filename then
      true
    else file_exists rest_fs full_path
  | ((node_name, node) :: rest_fs, dir_name :: rest_path) ->
    if node_name = dir_name then
      match node with
      | Dir contents -> file_exists contents rest_path
      | _ -> false
    else
      file_exists rest_fs full_path

let print_filesystem filesystem =

  let resolve_symlink_path path_to_symlink symlink_path =
    if file_exists filesystem (resolve path_to_symlink symlink_path) then
      symlink_path
    else
      ["NOT_FOUND"]
  in
  
  let rec print_dir path_here depth name contents =
    print_file depth ("/" ^ name);
    print_newline ();
    print_fs (name :: path_here) contents
  and

  print_element path_here name element =
    let depth = List.length path_here
    in
    match element with
    | File ->
      print_file depth name;
      print_newline ();
    | Dir contents -> print_dir path_here depth name contents
    | Symlink path ->
      let resolved_path =
        resolve_symlink_path (List.rev (name :: path_here)) path
      in
      print_symlink depth name resolved_path;
      print_newline ();
  and
    
  print_fs path_here = function
    | [] -> ()
    | (name, element) :: rest ->
      print_element path_here name element;
      print_fs path_here rest
  in
  print_fs [] filesystem

let example = [ "photos", Dir
    [ "march", Dir
        [ "photo_1.bmp", File ;
          "photo_2.bmp", File ;
          "photo_3.bmp", File ;
          "index.html", File ] ;
      "april", Dir
        [ "photo_1.bmp", File ;
          "photo_2.bmp", File ;
          "index.html", File ] ] ;
  "videos", Dir
    [ "video1.avi", File ;
      "video2.avi", File ;
      "video3.avi", File ;
      "video4.avi", File ;
      "best.avi", Symlink [ "video4.avi" ] ;
      "index.html", File ] ;
  "indexes", Dir
    [ "videos.html",
      Symlink [ ".." ; "videos" ; "index.html" ] ;
      "photos_march.html",
      Symlink [ ".." ; "photos" ; "march" ; "index.html" ] ;
      "photos_april.html",
      Symlink [ ".." ; "photos" ; "april" ; "index.html" ] ;
      "photos_may.html",
      Symlink [ ".." ; "photos" ; "may" ; "index.html" ] ] ]
