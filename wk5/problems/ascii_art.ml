type image = int -> int -> bool

type blend =
  | Image of image
  | And of blend * blend
  | Or of blend * blend
  | Rem of blend * blend

let all_white = fun x y -> false

let all_black = fun x y -> true

let checkers = fun x y -> y/2 mod 2 = x/2 mod 2

let square cx cy s = fun x y ->
  let minx = cx - s / 2 in
  let maxx = cx + s / 2 in
  let miny = cy - s / 2 in
  let maxy = cy + s / 2 in
  x >= minx && x <= maxx && y >= miny && y <= maxy

let disk cx cy r = fun x y ->
  let x' = x - cx in
  let y' = y - cy in
  (x' * x' + y' * y') <= r * r

let render_pixel img x y=
  let pixel = if img x y then "#" else " " in
  print_string pixel

let display_image width height img =
  for i = 0 to height do
    for j = 0 to width do
      render_pixel img j i
    done;
    print_newline ()
  done

(* "renders" a blend of images at given x, y coordinates *)
let rec render blend x y = match blend with
  | Image f -> f x y
  | And (a, b) -> (render a x y) && (render b x y)
  | Or (a, b) -> (render a x y) || (render b x y)
  | Rem (a, b) -> (render a x y) && not (render b x y)

let display_blend width height blend =
  display_image width height (render blend)
