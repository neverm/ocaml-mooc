let is_multiple i x = i mod x = 0

let output_multiples x n m =
  for i = n to m do
    if is_multiple i x then
      begin
        print_int i;
        print_string ","
      end
  done;
  (* overwrite last comma with a space and go back onto that space *)
  print_string "\b \b";
  print_newline ()

exception Found_zero

let display_sign_until_zero f m =
  try
    for i = 0 to m do
      let result = f i in
      if result = 0 then raise Found_zero
      else
        if result < 0 then print_endline "Negative"
        else print_endline "Positive"
    done
  with
  | Found_zero -> print_endline "Zero"

