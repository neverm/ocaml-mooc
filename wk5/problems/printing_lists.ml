let rec print_int_list = function
  | [] -> ()
  | x :: xs ->
    print_int x;
    print_newline ();
    print_int_list xs

let print_every_other k l =
  let rec inner i = function
    | [] -> ()
    | x :: xs when i != 0 && (k mod i) = 0 ->
      print_int x;
      print_newline ();
      inner (i + 1) xs
    | x :: xs -> inner (i + 1) xs
  in
  inner 0 l

let rec print_list print l = match l with
  | [] -> ()
  | x :: xs ->
    print x;
    print_newline ();
    print_list print xs
