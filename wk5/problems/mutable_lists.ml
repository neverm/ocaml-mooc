type 'a xlist =
  {mutable pointer: 'a cell }
and 'a cell =
  | Nil
  | List of 'a * 'a xlist

let nil () =
  { pointer = Nil }

let cons el rest =
  { pointer = List(el, rest) }

exception Empty_xlist

let head {pointer} = match pointer with
  | Nil -> raise Empty_xlist
  | List (element, rest) -> element

let tail {pointer} = match pointer with
  | Nil -> raise Empty_xlist
  | List (element, rest) -> rest

let add item list =
  let {pointer=rest_cell} = list in
  list.pointer <- List (item, {pointer=rest_cell})

let chop list =
  let {pointer=cell} = list in
  match cell with
  | Nil -> raise Empty_xlist
  | List (_, {pointer=rest}) ->
    list.pointer <- rest

let rec append xs ys =
  let {pointer=x_cell} = xs in
  match x_cell with
  | Nil ->
    let {pointer=y_cell} = ys in
    xs.pointer <- y_cell
  | List (_, rest) -> append rest ys

let rec filter p xs =
  let {pointer=cell} = xs in
  match cell with
  | Nil -> ()
  | List (v, ({pointer=rest} as ys)) ->
    if not (p v) then
      begin
      chop xs;
      filter p xs
    end
    else
      filter p ys

let l = { pointer =
    List (1, { pointer =
                 List (2, { pointer =
                              List (3, { pointer =
                                           Nil }) }) }) }
