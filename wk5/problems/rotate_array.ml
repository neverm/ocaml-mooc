(*
Using auxilary array is a bit dirty tactics, but whatever. Other options:
- use shorter aux array to only store n elements that need to be saved
   (too lazy to do it). Would save the space in case of rotating a big array
   by a small number
- use provided rotate function and call it n times (polynomial time worst case, so fuck it)
- abstract out in a different datatype that would map requested index into rotated index
   (a bit slower access but everything is in-place, ideal if the aren't many requests planned)

*)
let rotate_by a n =
  let len = Array.length a in
  let copy = Array.init len (fun i -> a.(i)) in
  for i = 0 to len-1 do
    a.(i) <- copy.((i+n) mod len)
  done

let rotate a = rotate_by a 1
