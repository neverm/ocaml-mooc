let read_intlist () =
  let l = ref [] in
  let doread () =
    try
      while true do
        l := (read_int ()) :: !l
      done
    with _ -> ()
  in
  doread();
  List.rev !l
    

let _ =
  let xs = read_intlist() in
  let result = List.fold_right (+) xs 0 in
  print_endline (string_of_int result)
