Example dune project with OUnit test framework tests

To use it, install required dependencies:

    opam install ounit dune

Tests should be run with `dune runtest tests/`
