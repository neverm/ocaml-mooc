(* Add code to test as a library here *)
(* open Lib.Trie *)
open OUnit

let suite = 
  "Tries" >::: [
    "test-pass" >:: ( fun _ ->
      (* some passing test *)
      let one = 1 in
      let two = 2 in
      assert_equal (one+two) 3
    );

    "test-fail" >:: ( fun _ ->
      (* some failing test *)
      let one = 1 in
      let two = 2 in
      assert_equal (one+two) 5
    );
    
  ]

let _ = run_test_tt_main suite
  
