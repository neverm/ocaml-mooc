(* Defining such a type is interesting because it allows the type-checker
to check that an integer is not used where an index is expected (or the converse). *)

type index = Index of int

let read a index = match index with
    | Index i -> a.(i);;

let inside a index = match index with
    | Index i -> i >= 0 && i < Array.length a;;

let next = function
    | Index n -> Index (n + 1);;

let min_index a =
    let rec aux i current_min current_min_idx =
        if not (inside a i) then current_min_idx
        else
        let current = read a i in
        if current < current_min
        then aux (next i) current i
        else aux (next i) current_min current_min_idx
    in
    aux (Index 0) a.(0) (Index 0);;
