open Lib.Trie
open OUnit


let example = Trie (None,
                    [('i', Trie (Some 11,
                                 [('n', Trie (Some 5, [('n', Trie (Some 9, []))]))]));
                     ('t', Trie (None,
                                 [('e',
                                   Trie (None,
                                         [('n', Trie (Some 12, [])); ('d', Trie (Some 4, []));
                                          ('a', Trie (Some 3, []))]))]));
                     ('A', Trie (Some 15, []))])

let suite = 
  "Tries" >::: [
    "test-children-from-char-empty" >:: ( fun _ ->
       let result = children_from_char [] 'a' in
       assert_equal None result
    );

    "test-children-from-char-not-found" >:: ( fun _ ->
       let Trie (_, ctc) = example in
       let result = children_from_char ctc 'e' in
       assert_equal None result
    );

    "test-children-from-char-found-first" >:: ( fun _ ->
       let Trie (_, ctc) = example in
       match ctc with
         | [] -> failwith "invalid example"
         | (_, t) :: _ ->
           let result = children_from_char ctc 'i' in
           assert_equal (Some t) result
    );

    "test-children-from-char-found-middle" >:: ( fun _ ->
       let Trie (_, ctc) = example in
       match ctc with
         | _:: (_, t) :: _ ->
           let result = children_from_char ctc 't' in
           assert_equal (Some t) result
         | _ -> failwith "invalid example"
    );

    "test-update-empty" >:: ( fun _ ->
       let result = update_children [] 'a' empty in
       assert_equal [('a', empty)] result
    );

    "test-update-nonempty" >:: ( fun _ ->
       let result = update_children [('b', empty)] 'a' empty in
       assert_equal [('b', empty); ('a', empty)] result
    );

    "test-update-replace" >:: ( fun _ ->
       let result = update_children [('a', empty)] 'a' example in
       assert_equal [('a', example)] result
    );

    "test-lookup-empty" >:: ( fun _ ->
       let result = lookup empty "smth" in
       assert_equal None result
    );

    "test-lookup-miss" >:: ( fun _ ->
       let result = lookup example "smth" in
       assert_equal None result
    );

    "test-lookup-hit" >:: ( fun _ ->
       let result = lookup example "ten" in
       assert_equal (Some 12) result
    );

    "test-insert-empty"  >:: ( fun _ ->
       let result = lookup (insert empty "hmm" 2) "hmm" in
       assert_equal (Some 2) result
    );

    "test-insert-nonempty"  >:: ( fun _ ->
       let result = lookup (insert example "hmm" 2) "hmm" in
       assert_equal (Some 2) result
    );

    "test-insert-replace"  >:: ( fun _ ->
       let result = lookup (insert (insert example "tea" 15) "tea" 5) "tea" in
       assert_equal (Some 5) result
    );
    
  ]

let _ = run_test_tt_main suite
  
