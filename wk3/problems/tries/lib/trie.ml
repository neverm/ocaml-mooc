type trie = Trie of int option * char_to_children
and char_to_children = (char * trie) list

let empty = Trie (None, [])

(* Get trie corresponding to the given char in the given char to trie mapping *)
let rec children_from_char m c = match m with
  | [] -> None
  | (ch, t) :: rest -> if ch = c then Some t else children_from_char rest c

(* Update given char to trie mapping with a new pair (c, t) if there is no existing mapping for c
   Otherwise, replace existing binding with (c, t) *)
let rec update_children m c t = match m with
  | [] -> [(c, t)]
  | (c', t') :: rest ->
    if c' = c then
    (c, t) :: rest else
    (c', t') :: update_children rest c t

let explode s =
  let rec expl i l =
    if i < 0 then l else
      expl (i-1) (s.[i] :: l) in
  expl (String.length s - 1) []

let implode chs =
  let result = Bytes.create (List.length chs) in
  let rec imp i = function
    | [] -> result
    | c :: chs' ->
      let _ = Bytes.set result i c in
      imp (i + 1) chs' in
  Bytes.to_string (imp 0 chs)

(* Lookup given word in the trie. Return None if the word is not in the trie, otherwise Some value *)
let rec lookup trie w =
  let Trie (value, children) = trie in match explode w with
  | [] -> value
  | c :: rest -> match children_from_char children c with
    | None -> None
    | Some t -> lookup t (implode rest)

(* Associate word with the value in the trie. Existing association will be replaced with the new one *)
let insert trie word value =
  let rec inner t i =
    let Trie (current_value, current_children) = t in
    let c = word.[i] in
    let is_end = (i = (String.length word - 1)) in
    (* Construct a node that will be associated with character c under current node *)
    let next_trie = match children_from_char current_children c with
      (* There is a node for current character, get that node *)
    | Some t' ->
      if is_end then
      (* Update value in the node *)
      let Trie (_, next_children) = t' in
      Trie (Some value, next_children) else
      (* Recursively insert rest of the string in the next node *)
      inner t' (i + 1)
      (* There is no node for current character, create a new node *)
    | None ->
      if is_end then
      (* Create an end node with the value to insert *)
      Trie (Some value, []) else
      (* Create an empty node and insert the rest of the string there *)
      inner empty (i + 1)
    in
    (* Update children of current node to have association from c to the newly created node *)
    Trie (current_value, update_children current_children c next_trie)
  in
  inner trie 0
