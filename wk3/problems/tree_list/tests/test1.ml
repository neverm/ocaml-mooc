(* Add code to test as a library here *)
open Lib.Clist
open OUnit

let example =
  CApp (CApp (CSingle 1,
              CSingle 2),
        CApp (CSingle 3,
              CApp (CSingle 4, CEmpty)))

let example2 =
  CApp (CApp (CSingle 1,
              CSingle 2),
        CApp (CApp (CSingle 5,
                    CSingle 7),
              CApp (CSingle 4, CEmpty))
       )

let suite = 
  "Tree-list" >::: [
    "test-to-list-empty" >:: ( fun _ ->
      assert_equal [] (to_list CEmpty)
    );

    "test-to-list-1-to-4" >:: ( fun _ ->
      assert_equal [1; 2; 3; 4] (to_list example)
    );

    "test-to-list-1-to-7" >:: ( fun _ ->
      assert_equal [1; 2; 5; 7; 4] (to_list example2)
        
    );

    "test-of-list-empty" >:: ( fun _ ->
      assert_equal [] (to_list (of_list []))
    );

    "test-of-list-1" >:: ( fun _ ->
      assert_equal [1] (to_list (of_list [1]))
    );
    
    "test-of-list-1-to-3" >:: ( fun _ ->
      assert_equal [1; 2; 3] (to_list (of_list [1; 2; 3]))
    ); 

    "test-append-to-empty" >:: ( fun _ ->
      let result = append (of_list []) (of_list [1; 2; 3]) in
      assert_equal [1; 2; 3] (to_list result)
    );

    "test-append-empty" >:: ( fun _ ->
      let result = (append (of_list [1; 2; 3]) (of_list [])) in
      assert_equal [1; 2; 3] (to_list result)
    );

    "test-append" >:: ( fun _ ->
      let result = (append (of_list [1; 2]) (of_list [3])) in
      assert_equal [1; 2; 3] (to_list result)
    );

    "test-head-empty" >:: ( fun _ ->
      assert_equal None (hd CEmpty)
    );

    "test-head-single" >:: ( fun _ ->
      assert_equal (Some 1) (hd (CSingle 1))
    );

    "test-head-tree" >:: ( fun _ ->
      assert_equal (Some 1) (hd example)
    );

    "test-tail-empty" >:: ( fun _ ->
      assert_equal None (tl CEmpty)
    );

    "test-tail-nonempty" >:: ( fun _ ->
      match (tl example) with
        | None -> failwith "empty tail"
        | Some tail ->
          assert_equal [2; 3; 4] (to_list tail)
           
    );   
  ]

let _ = run_test_tt_main suite
  
