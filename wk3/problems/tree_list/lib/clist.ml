type 'a clist =
  | CSingle of 'a
  | CApp of 'a clist * 'a clist
  | CEmpty

(* Convert clist to a list (recursive straight version) *)
let rec to_list2 l = match l with
  | CSingle a -> [a]
  | CEmpty -> []
  | CApp (left, right) -> (to_list2 left) @ (to_list2 right)

(* Convert clist to a list (iterative a bit more clever version) *)
let to_list l =
  let rec inner result tree = match tree with
  | CSingle a -> a :: result
  | CEmpty -> result
  | CApp (left, right) -> inner (inner result right) left
  in
  inner [] l

(* Convert a list into clist (which will look like a tree degenerated into a list) *)
let rec of_list l = match l with
  | [] -> CEmpty
  | a :: [] -> CSingle a
  | a :: rest -> CApp (CSingle a, (of_list rest))

(* Append one clist to another. This is a constant time operation and the whole point
   Of implementing this datastructure*)
let append l1 l2 = match (l1, l2) with
  | (CEmpty, t) -> t
  | (t, CEmpty) -> t
  | _ -> CApp (l1, l2)

(* Get head (first element) of the clist, None if the list is empty*)
let rec hd = function
  | CEmpty -> None
  | CSingle a -> Some a
  | CApp (left, _) -> hd left

(* Get tail (all elements except the first) of the clist *)
let rec tl = function
  | CEmpty -> None
  | CSingle _ -> None
  | CApp (left, right) -> match tl left with
    | None -> Some right
    | Some left_tail -> Some (append left_tail right)

let to_list_opt = function
  | None -> []
  | Some l -> to_list l
