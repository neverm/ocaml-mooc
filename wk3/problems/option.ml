let find a w =
    let rec find_aux i =
        if i >= Array.length a
        then None
        else if a.(i) = w then Some i else find_aux (i + 1)
    in
    find_aux 0;;

let default_int = function
  | None -> 0
  | Some n -> n;;

let merge a b = match (a, b) with
  | (None, None) -> None
  | (None, Some x) -> Some x
  | (Some x, None) -> Some x
  | (Some x, Some y) -> Some (x + y);;

let value =
    merge (Some 4) (Some 5);;

let to_str = function
  | Some(x) -> string_of_int x
  | None -> "Nada";;

print_string (to_str value);;