type queue = int list * int list;;

let is_empty (front, back) = match (front, back) with
  | ([], []) -> true
  | _ -> false;;

let enqueue x (front, back) = (front, x :: back);;

(* drop first n elemnts of a list, return remaining elements as a new list *)
let rec drop xs n = 
  if n = 0 then xs else match xs with
    | [] -> xs
    | x :: xs' -> drop xs' (n - 1);;

(* take first n elements of a list, return a new list *)
let rec take xs n =
  if n = 0 then [] else match xs with
    | [] -> xs
    | x :: xs' -> x :: take xs' (n - 1);;

(* 
  Write a function split : int list -> int list * int list such that
  split l = (front, back) where l = back @ List.rev front
  and the length of back and front is List.length l / 2 or List.length l / 2 + 1
 *)
let split l =
  let mid = (List.length l) / 2 in
  (List.rev (drop l mid), take l mid);;

let rec dequeue (front, back) =
  match front with
    | x :: xs' -> (x, (xs', back))
    | [] -> dequeue (split back)

