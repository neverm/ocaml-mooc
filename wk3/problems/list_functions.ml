(* mem x l is true if and only if x occurs in l *)

let rec mem x l = match l with
  | x' :: rest -> if x = x' then true else mem x rest
  | [] -> false;;

(* append l1 l2 is the concatenation of l1 and l2 *)
let rec append l1 l2 = match (l1, l2) with
  | ([], xs) -> xs
  | (x :: xs, ys) -> x :: append xs ys;;

(* combine l1 l2 is the list of pairs obtained by joining the elements of l1 and l2.
This function assumes that l1 and l2 have the same length.
For instance, combine [1;2] [3;4] = [(1, 3); (2, 4)] *)
let rec combine l1 l2 = match (l1, l2) with 
  | (x :: xs, y :: ys) -> (x, y) :: combine xs ys
  | _ -> [];;

(* assoc l k = Some x if (k, x) is the first pair of l whose first component is k.
If no such pair exists, assoc l k = None *)
let rec assoc l k = match l with
  | (key, value) :: rest -> if key = k then Some value else assoc rest k
  | _ -> None;;
