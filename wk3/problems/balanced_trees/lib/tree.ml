type 'a bt =
  | Empty
  | Node of 'a bt * 'a * 'a bt

let max a b = if a < b then b else a

let rec height t = match t with
  | Empty -> 0
  | Node (left, _, right) ->
    1 + max (height left) (height right)

(* 
  not optimal solution but easy to implement
  The easiest optimization would be to store the height directly in the node
  Other way could be going up the tree and returning early as soon as unbalanced node
  is detected
*)
let rec balanced t = match t with
  | Empty -> true
  | Node (left, _, right) ->
    (balanced left) && (balanced right)
