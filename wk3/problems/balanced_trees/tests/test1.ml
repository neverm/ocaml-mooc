(* Add code to test as a library here *)
open Lib.Tree
open OUnit

let suite = 
  "Trees" >::: [
    "test-height-empty" >:: ( fun _ ->
      assert_equal 0 (height Empty)
    );

    "test-height-three" >:: ( fun _ ->
      let tree = Node (Empty, 1, Node (Node (Empty, 2, Empty), 3, Empty)) in
      assert_equal 3 (height tree)
    );

    "test-balanced-empty" >:: ( fun _ ->
      assert_equal true (balanced Empty)
    );
 
    "test-balanced-false" >:: ( fun _ ->
      let tree = Node (Empty, 1, Node (Node (Empty, 2, Empty), 3, Empty)) in
      assert_equal false (balanced tree)
    );

    
    "test-balanced-node" >:: ( fun _ ->
      let tree = Node (Empty, 1, Empty) in
      assert_equal true (balanced tree)
    );

    "test-balanced-node" >:: ( fun _ ->
      let tree = Node (Node (Empty, 1, Empty), 1, Node (Empty, 1, Empty)) in
      assert_equal true (balanced tree)
    );
    
  ]

let _ = run_test_tt_main suite
  
