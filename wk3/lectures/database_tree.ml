type phone_number = int * int * int * int;;

type contact = {
  name: string;
  phone_number: phone_number
};;

type database =
  | NoContact
  | DataNode of database * contact * database;;

type query =
    | Insert of contact
    | Delete of contact
    | Update of contact
    | Search of string;;

type query_result =
    | Error
    | NewDatabase of database
    | FoundContact of contact;;

let search db name =
  let rec traverse = function
    | NoContact -> Error
    | DataNode (left, contact, right) ->
      if contact.name = name then
        FoundContact contact
      else if name < contact.name then
        traverse left
      else
        traverse right
  in
  traverse db;;

let insert db contact =
  let rec traverse t = match t with
    | NoContact -> DataNode(NoContact, contact, NoContact)
    | DataNode (left, contact', right) ->
      if contact.name = contact'.name then
        t
      else if contact.name < contact'.name then
        DataNode(traverse left, contact', right)
      else
        DataNode(left, contact', traverse right)
  in
  NewDatabase (traverse db)

let leila = {name = "leila"; phone_number = (0, 1, 2, 3)};;

let luke = {name = "luke"; phone_number = (1, 1, 2, 3)};;

let yoda = {name = "yoda"; phone_number = (2, 1, 2, 3)};;

let some_db = DataNode(DataNode(NoContact, leila, NoContact),
                       luke,
                       DataNode(NoContact, yoda, NoContact));;

let result = match search some_db "yoda" with
  | FoundContact _ -> "found"
  | _ -> "not found";;

print_string result;;