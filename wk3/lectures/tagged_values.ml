type phone_number = int * int * int * int;;

type contact = {
    name : string;
    phone_number: phone_number;
};;

type database = {
    number_of_contacts: int;
    contacts: contact array;
};;

type query =
    | Insert of contact
    | Delete of contact
    | Update of contact
    | Search of string;;

type query_result =
    | Error
    | NewDatabase of database
    | FoundContact of contact * int;;

let nobody = { name = ""; phone_number=(0, 0, 0, 0)};;

let make max_count =
    {
        number_of_contacts = 0;
        contacts = Array.make max_count nobody
    };;

let search db contact_name =
    let rec aux idx =
        if idx >= db.number_of_contacts then
            Error
        else if db.contacts.(idx).name = contact_name then
            FoundContact (db.contacts.(idx), idx)
        else
            aux (idx + 1)
    in
    aux 0;;

let insert db contact =
    if db.number_of_contacts >= Array.length db.contacts
    then Error
    else match (search db contact.name) with
        | Error ->
            (* this function either looks up contact by it's index number,
            or returns the contact to be inserted, in case the index is fist empty position*)
            let cells i =
            if i = db.number_of_contacts then contact
            else db.contacts.(i)
            in
            let db' = {
                number_of_contacts = db.number_of_contacts + 1;
                contacts = Array.init (Array.length db.contacts) cells
            }
            in
            NewDatabase(db')
        | _ -> Error;;

let delete db contact = match search db contact.name with
    | FoundContact (_, idx) ->
        let cells i = db.contacts.(if i >= idx && i < db.number_of_contacts then i + 1 else i)
        in
        let db' = {
          number_of_contacts = db.number_of_contacts - 1;
          contacts = Array.init (Array.length db.contacts) cells
        }
        in NewDatabase(db')
    | _ -> Error;;

let update db contact = match search db contact.name with
    | Error -> insert db contact
    | NewDatabase _ -> Error
    | FoundContact (_, _) -> match delete db contact with
        | NewDatabase db' -> insert db' contact
        | _ -> Error;;

let engine db query = match query with
    | Insert contact -> insert db contact
    | Delete contact -> delete db contact
    | Update contact -> update db contact
    | Search name -> search db name;;

let luke = { name = "luke"; phone_number=(1, 2, 3, 4)};;

let luke2 = { name = "luke"; phone_number=(5, 2, 3, 4)};;

let q1 = Insert luke;;

let q2 = Delete luke;;

let q3 = Search "luke";;

let db = make 5;;

let r1 = engine db q1;;

(* just a test function to find a contact by name and extract first phone number digit *)
let find_and_get_first_number db name = match search db name with
    | Error -> "user not found"
    | NewDatabase _ -> "I never asked for this"
    | FoundContact({phone_number = (d, _, _, _)}, _) -> string_of_int d

let result =  match r1 with
    | Error -> "error inserting"
    | FoundContact (_, _) -> "hmm did I ask for it?"
    | NewDatabase db -> match update db luke2 with
        | Error -> "error updating"
        | NewDatabase db' -> find_and_get_first_number db' "luke"
        | FoundContact(_, _) -> "hmmm";;

print_string result;;